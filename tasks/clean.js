/**
 * CLEAN
 * Deletes the compiled dst directory
 */
module.exports = function(config) {

  'use strict';

  // Util packages
  var gulp = require('gulp'),
      fs = require('fs');

  /*-------------------------------------------------------*\
  * Helpers
  \*-------------------------------------------------------*/

  /**
   * Delete folders recursive without an external plugin
   * http://www.geedew.com/remove-a-directory-that-is-not-empty-in-nodejs/
   */
  var deleteFolderRecursive = function(path) {
    if( fs.existsSync(path) ) {
      fs.readdirSync(path).forEach(function(file,index){
        var curPath = path + "/" + file;
        if(fs.lstatSync(curPath).isDirectory()) { // recurse
          deleteFolderRecursive(curPath);
        } else { // delete file
          fs.unlinkSync(curPath);
        }
      });
      fs.rmdirSync(path);
    }
  };

  /*-------------------------------------------------------*\
  * Task
  \*-------------------------------------------------------*/

  gulp.task('clean', function() {
    deleteFolderRecursive(config.dst);
  });

};