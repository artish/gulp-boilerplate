/**
 * BROWSERIFY
 * Bundle javascript files
 */
module.exports = function(config, transforms) {

  'use strict';

  // Util packages
  var gulp = require('gulp'),
      util = require('gulp-util'),
      gulpif = require('gulp-if'),
      pipe = require('multipipe'),
      glob = require('glob'),
      path = require('path');

  // Task packages
  var browserify = require('browserify'),
      watchify = require('watchify'),
      es = require('event-stream'),
      uglify = require('gulp-uglify'),
      source = require('vinyl-source-stream'),
      buffer = require('vinyl-buffer');

  // Browsersync
  var reload = require('browser-sync').reload;

  // Path variable
  var paths = config.paths.js;
  var transforms = paths.transforms;

  /*-------------------------------------------------------*\
  * Task
  \*-------------------------------------------------------*/

  gulp.task('scripts', function(done) {

    return glob(paths.src + paths.files, function(err, files) {
      var tasks = files.map(function(entry) {
        // Browserify options
        // When watch is enabled use watchify with caching
        var b = browserify({
          debug: !config.production,
          entries: [entry],
          cache: {},
          packageCache: {},
          fullPaths: !config.production,
        });
        if (config.watch) {
          var b = watchify(b);
        };

        // Apply the transformers
        transforms.forEach(function(element, index){
          b.transform(require(element));
        });

        var build = function() {
          var filename = path.basename(entry); // Get the filename without the src path
          var updateStart = Date.now(); // Current date for the duration calculation

          // Pipe Tasks
          return b.bundle()
            .on('error', function(error){
              config.errorHandler(error, 'Browserify')
            })
            .on('end', function() {
              util.log('Updating ' + util.colors.green(filename));
            })
            .pipe(source(filename)) // output filename
            .pipe(gulpif(config.production, pipe(
              buffer(),
              uglify()
            )))
            .pipe(gulp.dest(paths.dst))
            .on('end', function(){
              util.log('Done! ' + util.colors.green((Date.now() - updateStart) + 'ms'));
            })
            .pipe(reload({
              stream: true,
              once: true
            }));
        }

        b.on('update',build)
        return build();
      });
      es.merge(tasks).on('end', done);
    });

  });
};
