/**
 * JADE
 * Template compilation using Jade
 * http://jade-lang.com/
 */
module.exports = function(config) {

  'use strict';

  // Gulp & Utils
  var gulp = require('gulp'),
      plumber = require('gulp-plumber'),
      gulpif = require('gulp-if'),
      pipe = require('multipipe');

  // BrowserSync
  var browserSync = require('browser-sync');

  // Tasks
  var jade = require('gulp-jade'),
      data = require('gulp-data'),
      cacheBust = require('gulp-cache-bust');

  var paths = config.paths.templates;

  /*-------------------------------------------------------*\
  * Task
  \*-------------------------------------------------------*/

  var opts = {
    "jade": {
      pretty: !config.production
    },
    "cacheBust": {
      type: 'timestamp'
    }
  };

  gulp.task('templates', function () {
    return gulp.src(paths.src + paths.files)
    
      // Keep the pipe from breaking
      .pipe(plumber({
        errorHandler: function(err) {
          config.errorHandler(err, 'Jade');
          this.emit('end');
        }
      }))
      
      .pipe(data(config))
      
      .pipe(jade(opts.jade))

      // Cachebusting in production
      .pipe(gulpif(config.production, pipe(
         cacheBust(opts.cacheBust)
      )))

      .pipe(gulp.dest(paths.dst))
      .on('end', function() {
          browserSync.reload();
      });
  });

}