/**
 * LINTJS
 * Lint your javascript files
 */
module.exports = function(config) {

  'use strict';

  // Gulp & Utils
  var gulp = require('gulp');

  // Tasks
  var jshint = require('gulp-jshint'),
      stylish = require('jshint-stylish');

  var paths = config.paths.js;

  /*-------------------------------------------------------*\
  * Task
  \*-------------------------------------------------------*/

  gulp.task('lintjs', function() {
    return gulp.src(paths.src + paths.lintFiles)
      .pipe(jshint())
      .pipe(jshint.reporter(stylish))
      .on('error', function(error){config.errorHandler(error, 'LintJS')});
  });

};