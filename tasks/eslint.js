/**
 * LINTJS
 * Lint your javascript files
 */
module.exports = function(config) {

  'use strict';

  // Util packages
  var gulp = require('gulp');

  // Task packages
  var eslint = require('gulp-eslint');

  // Path variable
  var paths = config.paths.js;

  /*-------------------------------------------------------*\
  * Task
  \*-------------------------------------------------------*/

  gulp.task('lintjs', function() {
    return gulp.src([paths.src + '/**/**/**', '!node_modules/**'])
      .pipe(eslint())
      .pipe(eslint.format())
      .pipe(eslint.failAfterError());
  });

};
