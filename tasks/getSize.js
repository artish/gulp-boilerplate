/**
 * GETSIZE
 * Returns the size of the dst directory in KB
 * Practical for measuring banner sizes
 */
module.exports = function(config) {

  'use strict';

  // Util packages
  var gulp = require('gulp'),

  // Task packages
  var shell = require('gulp-shell');

  // Path variable
  var paths = config.paths.img;

  /*-------------------------------------------------------*\
  * Task
  \*-------------------------------------------------------*/

  // Returns the size of the built dir
  gulp.task('get-size', function() {
    return gulp.src(paths.dst, {read: false})
      // Return the real size of the dir, not just the size on the HD
      // http://superuser.com/a/101644
      // http://unix.stackexchange.com/questions/131073/awk-printf-number-in-width-and-round-it-up
      .pipe(shell([
        'echo "################################"',
        'echo "  FILESIZE"',
        'find dst -type f -print0 | xargs -0 stat -f %z | awk \'{t+= $1 / 1000}END{printf (\"  %1.0f KB\\n\", t)}\'',
        'echo "################################"'
      ]));
  });

};