/**
 * SVGMIN
 * Optimize your SVGs
 */
module.exports = function(config) {

  'use strict';

  // Gulp & Utils
  var gulp = require('gulp'),
      changed = require('gulp-changed');

  // Tasks
  var svgmin = require('gulp-svgmin');

  var paths = config.paths.svg;

  /*-------------------------------------------------------*\
  * Task
  \*-------------------------------------------------------*/

  gulp.task('svg', function () {
    return gulp.src(paths.src + '/*.svg')
      .pipe(changed(paths.dst))
      .pipe(svgmin())
      .pipe(gulp.dest(paths.dst));
  });

};
