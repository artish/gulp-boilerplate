/**
 * IMAGES
 * Optimize Images using imagemin
 */
module.exports = function(config) {

  'use strict';

  // Util packages
  var gulp = require('gulp'),
      changed = require('gulp-changed'),
      plumber = require('gulp-plumber');

  // Task packages
  var imagemin = require('gulp-imagemin');

  // Path variable
  var paths = config.paths.img;

  /*-------------------------------------------------------*\
  * Task
  \*-------------------------------------------------------*/

  gulp.task('images', function () {
    return gulp.src(paths.src + paths.files)
      .pipe(plumber())
      .pipe(changed(paths.dst))
      .pipe(imagemin({
        progressive: true,
        svgoPlugins: [{removeViewBox: false}],
        optimizationLevel: 3
      }))
      .on('error', function(error){config.errorHandler(error, 'Images')})
      .pipe(gulp.dest(paths.dst));
  });

};
