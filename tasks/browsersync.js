/**
 * BROWSERSYNC
 * Browser Liverload, Style injections and much more
 * http://www.browsersync.io/
 */
module.exports = function(config) {

  'use strict';

  // Util packages
  var gulp = require('gulp');

  // Browsersync
  var browserSync = require('browser-sync');

  /*-------------------------------------------------------*\
  * Task
  \*-------------------------------------------------------*/

  var opts = {
    // http://www.browsersync.io/docs/options/#option-ghostMode
    // ghostMode: {
    //     clicks: true,
    //     location: false,
    //     forms: true,
    //     scroll: false
    // },
    ghostMode: false,
    open: false,
    scrollThrottle: 10,

    // Default Port
    port: process.env.PORT || 3000,
    
    // Enable either BrowserSync Server or Proxy

    // ---------------------
    // Server when only html
    // ---------------------
    server: {
        baseDir: config.root
    }
    // ---------------------------
    // Virtual Server for PHP, etc
    // ---------------------------
    // proxy: {
    //  target: config.server
    //  // middleware: function (req, res, next) {
    //  //     console.log(req.url);
    //  //     next();
    //  // }
    // } 

  };

  gulp.task('browserSync', function() {
    browserSync(opts);
  });

  gulp.task('reload', function() {
    browserSync.reload();
  });

}