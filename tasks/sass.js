/**
 * SASS
 * Sass taks with sourcemaps, autoprefixer and live injections
 * Build: CSS-Shrink and Minification for smaller filesizes
 */
module.exports = function(config) {

  'use strict';

  // Gulp Requirements & Utilities
  var gulp = require('gulp'),
      path = require('path'),
      gulpFilter = require('gulp-filter'),
      rename = require('gulp-rename'),
      pipe = require('multipipe'),
      gulpif = require('gulp-if');

  // Browsersync
  var browserSync = require('browser-sync')

  // Task requirements
  var sass = require('gulp-sass'),
      plumber = require('gulp-plumber'),
      postcss = require('gulp-postcss'),
      autoprefixer = require('autoprefixer'),
      sourcemaps = require('gulp-sourcemaps'),
      cssshrink = require('gulp-cssshrink'),
      cssnano = require('gulp-cssnano');

  var paths = config.paths.css;

  /*-------------------------------------------------------*\
  * Task
  \*-------------------------------------------------------*/

  var opts = {
    sass: {
      errLogToConsole: true,
      outputStyle: config.production ? 'compressed' : 'nested'
    },
    autoprefixer: {
      browsers: [
        'last 2 versions',
        'ie 9',
        'ios 6',
        'android 4'
      ]
    }
  };

  gulp.task('sass', function () {

    return gulp.src(paths.src + paths.files)

      // Keep the pipe from breaking
      .pipe(plumber({
        errorHandler: function(err) {
          config.errorHandler(err, 'Sass');
          this.emit('end');
        }
      }))

      // Grab sourcemaps from source files
      .pipe(gulpif(!config.production, sourcemaps.init()))

      // Compile the sass
      .pipe(sass(opts.sass))

      // Write sourcemaps to the compiled css
      .pipe(gulpif(!config.production, sourcemaps.write({
        'includeContent': false,
        'sourceRoot': '.'
      })))

      // Post CSS Transforms
      .pipe(postcss([
        autoprefixer(opts.autoprefixer)
      ]))

      // Shrink and minify on build
      .pipe(gulpif(config.build, pipe(
        cssnano(),
        cssshrink()
      )))

      .pipe(gulp.dest(paths.dst))

      // BrowserSync Injection
      .pipe(browserSync.reload({stream:true}));

  });

}
