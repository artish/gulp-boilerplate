var Utils = function() {};

/**
 * Try to load a json file and return a correct error on failure
 * @param  {string} file Path to json
 * @return {obj}         Parsed json as an object
 */
Utils.prototype.requireJSON = function(file) {
  try {
    var loadedFile = require(file);
    return loadedFile;
  } catch(e) {
    if (e.code === 'MODULE_NOT_FOUND') {
      // When there is no gulpconfig.json
      console.log('Can\'t find ' + file + '!');
    } else if (e instanceof SyntaxError) {
      // When there is an error in the json document
      console.log('Error while parsing ' + file + '!');
    }
    return false;
  }
};

/**
 * Add a slash at the end of a word if there is none
 * @param {string} path
 */
Utils.prototype.addSlash = function(path) {
  if (path.slice(-1) !== '/') {
    return (path + '/');
  } else {
    return path;
  }
}

/**
 * Merge the src and dst paths
 * @param  {object} object Object to change values
 * @param  {string} target src, dst
 * @param  {object} base   config
 */
Utils.prototype.mergePaths = function(object, target, base) {
  if (object.hasOwnProperty(target)) {
    object[target] = this.addSlash(base[target]) + object[target];
  }
};

module.exports = new Utils();