var notifier = require('node-notifier');
var path = require('path');
var util = require('gulp-util');

module.exports = function(err, task) {

  task = typeof task !== 'undefined' ? task : 'Error';

  if (task !== 'Error') {
    var icon = 0;
    if (task ==='Browserify') {
      icon = path.join(__dirname, 'icons/browserify.png');
    };
  };

  notifier.notify({ 
    title: task,
    message: err.message,
    sound: true,
    icon: icon,
    wait: true,
    contentImage: 0
  });


  console.log();
  util.log(util.colors.red(task + ' Error:'));
  util.log(err.message);
  console.log();

  if (task === 'Browserify') {
    this.end;
  };

};