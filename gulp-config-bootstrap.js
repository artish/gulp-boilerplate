var _ = require('lodash'),
    path = require('path'),
    argv = require('yargs').argv,
    gu = require('./modules/utils');

var loadGulpConfig = function(userConfig) {

  var config, defaultConfig;
  userConfig = typeof userConfig !== 'undefined' ? userConfig : {}; // Optional userConfig argument

  // --------------
  // CONFIG LOADING
  // --------------


  var defaultConfig = gu.requireJSON(__dirname + '/defaults.gulpconfig.json');

  // Load the json if the userConfig variable is a string/path
  if (typeof userConfig === "string") {
    try {
      var userConfig = gu.requireJSON(path.join(process.cwd(), userConfig));
    } catch (e) {
      console.log('Error: Couldn\'t load custom config!');
    }
  };

  // Merge the userconfig with the default config
  config = _.merge(defaultConfig, userConfig);

  // -----------
  // MERGE PATHS
  // -----------
  // When mergepaths is set:
  // Append the src and dst paths to the path.src and path.dst
  if (config.mergepaths === true) {
    for (var key in config.paths) {
      gu.mergePaths(config.paths[key], 'src', config);
      gu.mergePaths(config.paths[key], 'dst', config);
    }
  }

  // ---------
  // ARGUMENTS
  // ---------
  config.production = !! argv.production;
  config.build = argv._.length ? argv._[0] === 'build' : false;
  config.watch = argv._.length ? argv._[0] === 'watch' : true;

  // -------------
  // TASK LOADER
  // -------------

  config.loadTask = function(tasks, config) {
    if (typeof tasks === 'string') {
      return require('./tasks/' + tasks)(config);
    } else {
      return tasks.forEach(function(x) {
        require('./tasks/' + x)(config);
      });
    }
  }

  // -------------
  // ERROR HANDLER
  // -------------

  config.errorHandler = require('./modules/errorHandler/errorHandler');

  return config;

}

module.exports = loadGulpConfig;